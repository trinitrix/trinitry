# Copyright (C) 2024 - 2024:
# The Trinitrix Project <bpeetz@b-peetz.de, antifallobst@systemausfall.org>
# SPDX-License-Identifier: LGPL-3.0-or-later
#
# This file is part of the Trinitry crate for Trinitrix.
#
# Trinitry is free software: you can redistribute it and/or modify
# it under the terms of the Lesser GNU General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# and the Lesser GNU General Public License along with this program.
# If not, see <https://www.gnu.org/licenses/>.
{
  description = "A very simple programming language, used to map functions to commands";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

    treefmt-nix = {
      url = "github:numtide/treefmt-nix";
      inputs = {
        nixpkgs.follows = "nixpkgs";
      };
    };

    crane = {
      url = "github:ipetkov/crane";
      inputs = {
        nixpkgs.follows = "nixpkgs";
      };
    };
    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
      };
    };

    # inputs for following
    systems = {
      url = "github:nix-systems/x86_64-linux"; # only evaluate for this system
    };
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
    flake-utils = {
      url = "github:numtide/flake-utils";
      inputs = {
        systems.follows = "systems";
      };
    };
  };

  outputs = {
    self,
    nixpkgs,
    flake-utils,
    treefmt-nix,
    crane,
    rust-overlay,
    ...
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = import nixpkgs {
        inherit system;
        overlays = [(import rust-overlay)];
      };

      nightly = false;
      rust_minimal =
        if nightly
        then pkgs.rust-bin.selectLatestNightlyWith (toolchain: toolchain.minimal)
        else pkgs.rust-bin.stable.latest.minimal;
      rust_default =
        if nightly
        then pkgs.rust-bin.selectLatestNightlyWith (toolchain: toolchain.default)
        else pkgs.rust-bin.stable.latest.default;

      craneLib = (crane.mkLib pkgs).overrideToolchain rust_minimal;
      craneBuild = craneLib.buildPackage {
        src = craneLib.cleanCargoSource ./.;

        doCheck = true;
      };

      treefmtEval = import ./treefmt.nix {inherit treefmt-nix pkgs;};
    in {
      packages.default = craneBuild;

      checks = {
        inherit craneBuild;
        formatting = treefmtEval.config.build.check self;
      };

      formatter = treefmtEval.config.build.wrapper;

      devShells.default = pkgs.mkShell {
        packages = with pkgs; [
          cocogitto

          yq

          rust_default
          cargo-edit

          licensure
        ];
      };
    });
}
# vim: ts=2

