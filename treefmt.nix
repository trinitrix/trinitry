# Copyright (C) 2024 - 2024:
# The Trinitrix Project <bpeetz@b-peetz.de, antifallobst@systemausfall.org>
# SPDX-License-Identifier: LGPL-3.0-or-later
#
# This file is part of the Trinitry crate for Trinitrix.
#
# Trinitry is free software: you can redistribute it and/or modify
# it under the terms of the Lesser GNU General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# and the Lesser GNU General Public License along with this program.
# If not, see <https://www.gnu.org/licenses/>.
{
  treefmt-nix,
  pkgs,
}:
treefmt-nix.lib.evalModule pkgs (
  {pkgs, ...}: {
    # Used to find the project root
    projectRootFile = "flake.nix";

    programs = {
      alejandra.enable = true;
      rustfmt.enable = true;
      clang-format.enable = true;
      mdformat.enable = true;
      shfmt = {
        enable = true;
        indent_size = 4;
      };
      shellcheck.enable = true;
      prettier = {
        settings = {
          arrowParens = "always";
          bracketSameLine = false;
          bracketSpacing = true;
          editorconfig = true;
          embeddedLanguageFormatting = "auto";
          endOfLine = "lf";
          # experimentalTernaries = false;
          htmlWhitespaceSensitivity = "css";
          insertPragma = false;
          jsxSingleQuote = true;
          printWidth = 80;
          proseWrap = "always";
          quoteProps = "consistent";
          requirePragma = false;
          semi = true;
          singleAttributePerLine = true;
          singleQuote = true;
          trailingComma = "all";
          useTabs = false;
          vueIndentScriptAndStyle = false;

          tabWidth = 4;
          overrides = {
            files = ["*.js"];
            options.tabwidth = 2;
          };
        };
      };
      stylua.enable = true;
      ruff = {
        enable = true;
        format = true;
      };
      taplo.enable = true;
    };

    settings = {
      global.excludes = [
        "CHANGELOG.md"
        "NEWS.md"
      ];
      formatter = {
        clang-format = {
          options = ["--style" "GNU"];
        };
        shfmt = {
          includes = ["*.bash"];
        };
      };
    };
  }
)
