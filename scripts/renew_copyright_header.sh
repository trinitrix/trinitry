#! /usr/bin/env sh
# Copyright (C) 2024 - 2024:
# The Trinitrix Project <bpeetz@b-peetz.de, antifallobst@systemausfall.org>
# SPDX-License-Identifier: LGPL-3.0-or-later
#
# This file is part of the Trinitry crate for Trinitrix.
#
# Trinitry is free software: you can redistribute it and/or modify
# it under the terms of the Lesser GNU General Public License as
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# and the Lesser GNU General Public License along with this program.
# If not, see <https://www.gnu.org/licenses/>.

# NOTE: This is the line length of the .licensure.yml header template **plus** the extra
# line after the template comment.
TEMPLATE_LINE_LENGTH=20
LATEX_TEMPLATE_LINE_LENGTH=9

PROJECT_ROOT="$(git rev-parse --show-toplevel)"

remove() {
    extension="$1"
    file="$2"

    # We need to differentiate, when removing the old copyright header, as some
    # formatters do weird things to the file
    case "$extension" in
    # normal '#' comments (these are $TEMPLATE_LINE_LENGTH lines long)
    "Makefile" | "toml" | "envrc" | "yml" | "gitignore" | "awk")
        sed --in-place "1,${TEMPLATE_LINE_LENGTH}d" "$file"
        ;;
    # LaTeX files (or TeX files in general) have a different license, use the
    # $LATEX_TEMPLATE_LINE_LENGTH variable.
    "tex")
        sed --in-place "1,${LATEX_TEMPLATE_LINE_LENGTH}d" "$file"
        ;;
    # normal '/* ... */' like comments (these are $TEMPLATE_LINE_LENGTH + 2 lines long)
    "c" | "h" | "md" | "rs")
        length="$((TEMPLATE_LINE_LENGTH + 2))"
        sed --in-place "1,${length}d;" "$file"
        ;;
    # alejandra (the nix formatter) removes the blank line after the comment,
    # thus only $TEMPLATE_LINE_LENGTH - 1 lines
    "nix")
        length="$((TEMPLATE_LINE_LENGTH - 1))"
        sed --in-place "1,${length}d;" "$file"
        ;;
    # Shell needs a shebang on the first line, only after the first line can we
    # remove the $TEMPLATE_LINE_LENGTH lines
    "sh")
        sed --in-place "2,${TEMPLATE_LINE_LENGTH}d;" "$file"
        licensure --in-place "$file"

        TEMPLATE_LINE_LENGTH_NEW="$(($(yq --raw-output '.licenses | map(.template) | join("")' "$PROJECT_ROOT/.licensure.yml" | wc -l) + $(yq '.comments | last | .commenter.trailing_lines' "$PROJECT_ROOT/.licensure.yml")))"

        # delete the current shebang
        to="$((TEMPLATE_LINE_LENGTH_NEW + 1))"
        sed --in-place "${TEMPLATE_LINE_LENGTH_NEW},${to}d;" "$file"

        # add a new one
        sed --in-place "1i#! /usr/bin/env sh" "$file"
        ;;
    *)
        echo "File '$file' with extension '$extension' is not know yet, please add it!"
        ;;
    esac
}

list() {
    echo "$extension -> $file"
}

if [ -f "$1" ]; then
    file="$(realpath "$1")"
    filename="$(basename -- "$file")"
    extension="${filename##*.}"
    filename="${filename%.*}"

    if [ -n "$DRY_RUN" ]; then
        list "$extension" "$file"
    else
        remove "$extension" "$file"
    fi
else
    fd --type file --hidden . | while read -r file; do
        if grep --quiet 'SPDX-License-Identifier' "$file"; then
            filename="$(basename -- "$file")"
            extension="${filename##*.}"
            filename="${filename%.*}"

            if [ -n "$DRY_RUN" ]; then
                list "$extension" "$file"
            else
                remove "$extension" "$file"
            fi
        fi
    done

    if [ -z "$DRY_RUN" ]; then
        licensure --in-place --project
        nix fmt
    fi
fi
